# -*- coding: utf-8 -*-
import logging
from openerp.osv import osv
from openerp.tools.safe_eval import safe_eval

_logger = logging.getLogger(__name__)


class IrCron(osv.Model):
    _inherit = 'ir.cron'

    def do_manual_execution(self, cr, uid, ids, context=None):
        for ir_cron in self.browse(cr, uid, ids, context=context):
            #try:
                method = getattr(self.pool.get(ir_cron.model), ir_cron.function)
                args = safe_eval(ir_cron.args)
                method(cr, uid, *args)
            #except AttributeError as e:
            #    raise osv.except_orm("Error", "No existe atributo %s en %s: %s" % (ir_cron.function, ir_cron.model, str(e)))
            #except Exception as e:
            #    _logger.info("No se pudo ejecutar el metodo %s: %s" % (ir_cron.function, str(e)))

# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
from tools.translate import _
import jasper_reports


def reportjasper_document( cr, uid, ids, data, context ):
    return {
        'parameters': {    
            'id': data['id'],
        }
   }


class account_invoice_ext(osv.osv):

    _inherit = 'account.invoice.ext'

    _columns={              
              'number':fields.char('Número comprobante', size=17, readonly=True),  
              'numdocmodificado':fields.char('factura rectificada', size=128, readonly=True),
              'user_id':fields.many2one('res.users', 'Usuario', required=False),
              'company_id':fields.many2one('res.company', 'Compañia', required=False),     
              'type_ref':fields.char('type_ref', size=5),          
              'type':fields.selection([
                  ('factura','Factura'),
                  ('ncredito','Nota de crédito'),
                  ('ndebit','Nota de débito'),
                   ],    'Tipo', select=True, readonly=True),
              'fechaemision': fields.date('Fecha emisión'), 
              'fechaemisiondocsustento':fields.date('Fecha emisión Doc Sustento', size=128, readonly=True), 
              'direstablecimiento': fields.char('Dirección',states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}, size=128),
              'identificacioncomprador': fields.char('RUC/CI',readonly=True, size=128),
              'totaldescuento': fields.float('Descuento', digits=(16, 2), readonly=True),
              'importetotal': fields.float('Valor total', digits=(16, 2), readonly = True),
              'totalsinimpuestos': fields.float('Subtotal', digits=(16, 2), readonly = True),
              'valormodificacion': fields.float('Valor modificado', digits=(16, 2), readonly = True),
              'subtotal_0': fields.float('Subtotal 0%', digits=(16, 2), readonly = True),
              'subtotal_12': fields.float('Subtotal %', digits=(16, 2), readonly = True),
              'subtotal_nosujeto': fields.float('Subtotal No suj.', digits=(16, 2), readonly = True),
              'iva': fields.float('Valor Iva', digits=(16, 2), readonly = True),
              'partner_id':fields.many2one('res.partner', 'Cliente', readonly=True),
              'invoice_line_ext': fields.one2many('account.invoice.line.ext', 'invoice_ext_id', 'Detalles', states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'note': fields.text('Otra información'), 
              'name_file':fields.char('nombre_archivo', size=128, readonly=True),
              'guide_remision':fields.char('Guía de remisión', size=128, readonly=True),
              'motivo':fields.char('Razon de modificación', size=128, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'partner_name':fields.char('Cliente',size=128, readonly=True),
              'partner_email':fields.char('email',size=128, readonly=True),
              'observacion1':fields.char('Observacion 1', size=256, required=False, readonly=True),
              'other_guides':fields.char('Guias de remisión', size=128, readonly=True),
              'observacion2':fields.char('Observacion 1', size=256, required=False, readonly=True),
        'coddocreemb': fields.char('coddocreemb', readonly=True),
        'totalcomprobantesreembolso': fields.char('totalcomprobantesreembolso', readonly=True),
        'totalbaseimponiblereembolso': fields.char('totalbaseimponiblereembolso', readonly=True),
        'totalimpuestoreembolso': fields.char('totalimpuestoreembolso', readonly=True),
        'invoice_line_reembolso_ext': fields.one2many('account.invoice.line.reembolso.ext', 'invoice_ext_id',
                                                      'Detalles Reembolso', readonly=True),
        'is_reemb': fields.boolean('Es reembolso', readonly=False),
              }
    
    _order = "number desc"
    _rec_name = 'number'
    _defaults = {
        'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'state': 'loaded',
        'is_reemb': False
            }
    
    def unlink(self, cr, uid, ids, context=None):
        if not context:
            context={}        
        for invoice in self.browse(cr , uid ,ids):
            if invoice.state in ('authorized', 'received', 'key_contingency'):
                raise osv.except_osv(_('Error!'), _("No puedes eliminar comprobantes que ya esten autorizados, que esten esperando autorización o que se emitan con claves de contingencia" ))
            docs = self.pool.get('ir.attachment').search(cr, uid ,[('res_id','=',invoice.id),('res_model','=','account.invoice.ext')])
            cr.autocommit(True)
            if docs:
                cr.execute('delete from ir_attachment where id IN %s', (tuple(docs),))
        res = super(account_invoice_ext, self).unlink(cr, uid, ids, context)       
        return res 

    def imprimirReporte(self, cr, uid, ids, context=None):
        data = {}
        data['model'] = 'account.invoice.ext'
        data['id'] = ids[0]
        data['jasper'] = {
                       'id':ids[0]
                      }
        data['type'] = context.get('type')
        reporte = 'reportjasper_'+data['type']
        invoice = self.browse(cr, uid, ids[0])
        if invoice.is_reemb and data['type']=='factura':
            reporte = 'reportjasper_factura_reembolso'
        jasper_reports.report_jasper(
            'report.'+reporte,
            'account.invoice.ext',
            parser=reportjasper_document
        )
        
        return{
               'type':'ir.actions.report.xml',
               'report_name':reporte,
               'datas':data
               }

    def action_invoice_ext_sent(self, cr, uid, ids, context=None):
        if not context: context= None
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')
        invoice = self.browse(cr , uid, ids[0])
        if invoice.type =='factura':
            template='email_template_edi_account_invoice_ext'
        elif invoice.type =='ncredito':
            template='email_template_edi_account_invoice_nc_ext'
        elif invoice.type =='ndebit':
            template='email_template_edi_account_invoice_nd_ext'
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 'voucher_electronic', template)[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(context)
        ctx.update({
            'default_model': 'account.invoice.ext',
            'default_res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_invoice_as_sent': True,
            })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


class account_invoice_line_reembolso_ext(osv.osv):
    _name = "account.invoice.line.reembolso.ext"

    _columns = {
        'invoice_ext_id': fields.many2one('account.invoice.ext', 'Invoice Reference', ondelete='cascade', select=True),
        'tipoidentificacionproveedorreembolso': fields.char('tipoidentificacionproveedorreembolso', readonly=True),
        'identificacionproveedorreembolso': fields.char('identificacionproveedorreembolso', readonly=True),
        'codpaispagoproveedorreembolso': fields.char('codpaispagoproveedorreembolso', readonly=True),
        'tipoproveedorreembolso': fields.char('tipoproveedorreembolso', readonly=True),
        'coddocreembolso': fields.char('coddocreembolso', readonly=True),
        'estabdocreembolso': fields.char('estabdocreembolso', readonly=True),
        'ptoemidocreembolso': fields.char('ptoemidocreembolso', readonly=True),
        'secuencialdocreembolso': fields.char('secuencialdocreembolso', readonly=True),
        'fechaemisiondocreembolso': fields.char('fechaemisiondocreembolso', readonly=True),
        'numeroautorizaciondocreemb': fields.char('numeroautorizaciondocreemb', readonly=True),
        'codigo': fields.char('codigo', readonly=True),
        'codigoporcentaje': fields.char('codigoporcentaje', readonly=True),
        'tarifa': fields.char('tarifa', readonly=True),
        'baseimponiblereembolso': fields.char('baseimponiblereembolso', readonly=True),
        'impuestoreembolso': fields.char('impuestoreembolso', readonly=True),
    }


account_invoice_line_reembolso_ext()


class account_invoice_line_ext(osv.osv):
    _name = "account.invoice.line.ext"
    _columns={
              'invoice_ext_id': fields.many2one('account.invoice.ext', 'Invoice Reference', ondelete='cascade', select=True),
              'codigoprincipal':fields.char('Cod. Principal', size=250, required=True),
              'codigointerno':fields.char('Cod. Interno', size=250, required=True), 
              'descripcion':fields.char('Descripción', size=300, required=True), 
              'cantidad': fields.integer('Cant', required=True), 
              'preciounitario': fields.float('Precio Unit', digits=(16, 2),required=True), 
              'descuento': fields.float('Descuento', digits=(16, 2)),      
              'preciototalsinimpuesto': fields.float('Precio Total', digits=(16, 2),required=True ),
              'codigoporcentaje':fields.char('codigoPorcentaje', size=4, required=True),
              }

account_invoice_line_ext()
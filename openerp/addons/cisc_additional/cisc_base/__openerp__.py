# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2013  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

{
    "name": "CISC Ajustes base",
    "version": "1.0",
    "depends": ["base",
                "email_template",
                "web_adblock",
                "cisc_verifica_ruc_ci",
                "cisc_states",
                "document",
                ],
                
    "author": "Jairo Troncoso jairom0704@gmail.com",
    "website" : "https://twitter.com/jairomt25",
    "category": "Partners",
    "complexity": "normal",
    "description": """
    This module provide :
    
    """,
    "init_xml": [],
    'update_xml': [
                   "data/data.xml",
                   #"views/notification_view.xml",                  
                   #"views/partner_import_view.xml",
                   #"views/account_view.xml",    
                   ],
    'qweb' : [
                #"static/src/base.xml",
                "static/src/admin.xml",],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}